/*
 * Modify this file to change what commands output to your statusbar, and
 * recompile using the make command.
 */

static const Block blocks[] = {
     /* icon | command | update | interval | update | signal */

    {"", "get-battery-cap",    10, 0},
    {"", "get-mem-usage",      10, 0},
    {"", "get-weather",     43200, 0},
    {"", "get-epoch",          50, 0},
};

/*
 * sets delimeter between status commands.
 * NULL character ('\0') means no delimeter
 */

static char delim[] = "  ";
static unsigned int delimLen = 5;
