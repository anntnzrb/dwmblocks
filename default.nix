{ pkgs ? import <nixpkgs>  {}}:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    gcc
    xorg.libX11 xorg.libXft xorg.libXinerama xorg.xev
  ];

  shellHook = ''
   printf "=> nix-shell: setup for dwmblocks\n"
  '';
}
